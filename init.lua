if minetest.settings:get_bool("Eurones") == true then
	dofile(minetest.get_modpath("moneyroo").."/eurones.lua")
end

if minetest.settings:get_bool("Poundees") == true then
	dofile(minetest.get_modpath("moneyroo").."/poundees.lua")
end

if minetest.settings:get_bool("Dollaroos") == true then
	dofile(minetest.get_modpath("moneyroo").."/dollaroos.lua")
end