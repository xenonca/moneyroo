local S
if minetest.get_translator ~= nil then
    S = minetest.get_translator(minetest.get_current_modname())
else
    S = function(str)
        return(str)
    end
end

minetest.register_craftitem("moneyroo:eurones_5", {
    description = S("5 Eurones"),
	inventory_image = "moneyroo_eurones_5.png",
	groups = {flammable = 1},
})

minetest.register_craftitem("moneyroo:eurones_10", {
    description = S("10 Eurones"),
	inventory_image = "moneyroo_eurones_10.png",
	groups = {flammable = 1},
})

minetest.register_craftitem("moneyroo:eurones_20", {
    description = S("20 Eurones"),
	inventory_image = "moneyroo_eurones_20.png",
	groups = {flammable = 1},
})

minetest.register_craftitem("moneyroo:eurones_50", {
    description = S("50 Eurones"),
	inventory_image = "moneyroo_eurones_50.png",
	groups = {flammable = 1},
})

minetest.register_craftitem("moneyroo:eurones_100", {
    description = S("100 Eurones"),
	inventory_image = "moneyroo_eurones_100.png",
	groups = {flammable = 1},
})

minetest.register_craftitem("moneyroo:eurones_200", {
    description = S("200 Eurones"),
	inventory_image = "moneyroo_eurones_200.png",
	groups = {flammable = 1},
})

minetest.register_craftitem("moneyroo:eurones_500", {
    description = S("500 Eurones"),
	inventory_image = "moneyroo_eurones_500.png",
	groups = {flammable = 1},
})

---

minetest.register_craft({
	type = "fuel",
	recipe = "moneyroo:eurones_5",
	burntime = 3,
})
 
minetest.register_craft({
	type = "fuel",
	recipe = "moneyroo:eurones_10",
	burntime = 3,
})

minetest.register_craft({
	type = "fuel",
	recipe = "moneyroo:eurones_20",
	burntime = 3,
})

minetest.register_craft({
	type = "fuel",
	recipe = "moneyroo:eurones_50",
	burntime = 3,
})

minetest.register_craft({
	type = "fuel",
	recipe = "moneyroo:eurones_100",
	burntime = 3,
})

minetest.register_craft({
	type = "fuel",
	recipe = "moneyroo:eurones_200",
	burntime = 3,
})

minetest.register_craft({
	type = "fuel",
	recipe = "moneyroo:eurones_500",
	burntime = 3,
}) 
